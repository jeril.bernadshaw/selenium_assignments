package Demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectMethodsOnSortBy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	WebDriver driver=new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/");
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Books')]"));
		WebElement element=driver.findElement(By.id("products-orderby"));
		Select sl=new Select(element);
		sl.selectByVisibleText("Price: Low to High");
		//s.deselectByValue("https://demowebshop.tricentis.com/books?orderby=10");
		//s.deselectByIndex("3");
		
	}

}
