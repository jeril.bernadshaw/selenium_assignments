package Demo;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WorkingOnMultipleWebElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
         WebDriver driver=new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		//WebElement ele=driver.findElements(By.tagName("s"));

		List<WebElement> links=driver.findElements(By.tagName("a"));
		
		System.out.println(links.size());
		
		
		for(int i=0;i<links.size();i++)
		{
			String TextLink=links.get(i).getText();
			System.out.println(TextLink);
		}
	}

}
