package AssignmentDemoWebShop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Computer {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys("jerilreegan@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Jeril@2807");
		driver.findElement(By.id("RememberMe")).click();
		
		driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
		WebElement computer = driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
		Actions act = new Actions(driver);
		act.moveToElement(computer).build().perform();
		act.moveToElement(driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Desktop')]")))
				.click().perform();
		WebElement element = driver.findElement(By.id("products-orderby"));
		Select sel = new Select(element);
		sel.selectByVisibleText("Price: Low to High");
		driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
		Thread.sleep(3000);

		driver.findElement(By.xpath("(//input[@type=\"button\"])[2]")).click();
		driver.findElement(By.partialLinkText("Shopping cart")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("PickUpInStore")).click();
		driver.findElement(By.xpath("(//input[@type='button'])[3]")).click();
		
		Thread.sleep(3000);

		driver.findElement(By.xpath("(//input[@type='button'])[5]")).click();
		Thread.sleep(3000);

		driver.findElement(By.xpath("(//input[@type='button'])[6]")).click();
		Thread.sleep(3000);

		driver.findElement(By.xpath("(//input[@type='button'])[7]")).click();
		Thread.sleep(3000);

		String text = driver.findElement(By.xpath("//strong[text()='Your order has been successfully processed!']"))
				.getText();
		System.out.println(text);

	}

}
