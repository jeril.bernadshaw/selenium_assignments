package AssignmentOrangeHRM;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AssignmentAdmin {

	public static void main(String[] args) throws AWTException, InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		driver.findElement(By.name("username")).sendKeys("Admin");
		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();

		driver.findElement(By.xpath("(//span[@class=\"oxd-text oxd-text--span oxd-main-menu-item--name\"])[1]"))
				.click();
		driver.findElement(By.xpath("(//div//input[@class='oxd-input oxd-input--active'])[2]")).sendKeys("Jeril");
		driver.findElement(By.xpath("(//div[@class='oxd-select-text-input'])[1]")).click();

		Robot rs = new Robot();
		rs.keyPress(KeyEvent.VK_DOWN);
		rs.keyRelease(KeyEvent.VK_DOWN);
		rs.keyPress(KeyEvent.VK_ENTER);
		rs.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@placeholder=\"Type for hints...\"]")).sendKeys("c");
		Thread.sleep(3000);

		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(2000);

		r.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(2000);

		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		driver.findElement(By.xpath("(//div[@tabindex='0'])[2]")).click();
		Thread.sleep(3000);

		Robot r1 = new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		r1.keyPress(KeyEvent.VK_ENTER);
		r1.keyRelease(KeyEvent.VK_ENTER);

		driver.findElement(By.xpath("(//button[@type='button' ])[4]")).click();
		driver.findElement(By.xpath("(//div[text()='-- Select --'])[1]")).click();
		Robot r2 = new Robot();
		r2.keyPress(KeyEvent.VK_DOWN);
		r2.keyRelease(KeyEvent.VK_DOWN);
		r2.keyPress(KeyEvent.VK_DOWN);
		r2.keyRelease(KeyEvent.VK_DOWN);

		r2.keyPress(KeyEvent.VK_ENTER);
		r2.keyRelease(KeyEvent.VK_ENTER);

		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@placeholder=\"Type for hints...\"]")).sendKeys("r");
		Robot r4 = new Robot();
		r4.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(2000);

		r4.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(2000);

		r4.keyPress(KeyEvent.VK_ENTER);
		r4.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);

		driver.findElement(By.xpath("(//div[@class='oxd-select-text-input'])[2]")).click();
		Robot r3 = new Robot();
		r3.keyPress(KeyEvent.VK_DOWN);
		r3.keyRelease(KeyEvent.VK_DOWN);
		r3.keyPress(KeyEvent.VK_DOWN);
		r3.keyRelease(KeyEvent.VK_DOWN);

		r3.keyPress(KeyEvent.VK_ENTER);
		r3.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);

		driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[2]")).sendKeys("Jeril");
		Thread.sleep(2000);

		driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("Jeril@123");
		Thread.sleep(2000);

		driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("Jeril@123");
		Thread.sleep(2000);

		driver.findElement(By.xpath("//button[@type='submit']")).click();

	
	}

}
